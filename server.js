const http = require("http");
const route = require("./router");

const server = http.createServer((request, response) => {
    if (request.method === "GET") {
        const url = request.url;
        const urlParameters = url.split("/");
        const delayParameter = Number(urlParameters[urlParameters.length - 1]);
        const statusCode = delayParameter;

        switch (url) {
            case "/":
                route.renderHtml(request, response);
                break;
            case "/style.css":
                route.renderCss(request, response);
                break;
            case "/html":
                route.renderHtml(request, response);
                break;
            case "/json":
                route.renderJson(request, response);
                break;
            case "/uuid":
                route.renderUuid(request, response);
                break;
            case `/status/${statusCode}`:
                route.renderStatusCode(request, response, statusCode);
                break;
            case `/delay/${delayParameter}`:
                route.renderDelayResponse(request, response, delayParameter);
                break;
            default:
                route.render404NotFound(request, response);
                break;
        }
    }
});
server.listen(8000);
