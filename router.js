const fs = require("fs");
const uuid = require("uuid");
const http = require("http");

const renderHtml = (request, response) => {
    let filename = '';
    if (request.url === "/") {
        filename = "./index.html";
    }
    if (request.url === "/html") {
        filename = "./get-html.html";
    }
    readFilePromise(filename)
        .then((data) => {
            response.writeHead(200, {
                "Content-Type": "text/html",
            });
            response.write(data);
            response.end();
        })
        .catch((error) => {
            handleResourceNotFoundError(request, response, error);
            response.end();
        });
};

const renderCss = (request, response) => {
    readFilePromise("./style.css")
        .then((data) => {
            response.writeHead(200, {
                "Content-Type": "text/css",
            });
            response.write(data);
            response.end();
        })
        .catch((error) => {
            handleResourceNotFoundError(request, response, error);
            response.end();
        });
};

const renderJson = (request, response) => {
    readFilePromise("./data.json")
        .then((data) => {
            response.writeHead(200, {
                "Content-Type": "application/json",
            });
            response.write(data);
            response.end();
        })
        .catch((error) => {
            handleResourceNotFoundError(request, response, error);
            response.end();
        });
};

const renderUuid = (request, response) => {
    const UUID4 = uuid.v4();
    if (UUID4 !== undefined) {
        const data = {
            "uuid": UUID4
        };
        response.writeHead(200, {
            "Content-Type": "application/json",
        });
        response.write(JSON.stringify(data));
        response.end();
    } else {
        const error = "Error in generating UUID";
        handleResourceNotFoundError(request, response, error);
        response.end();
    }
};

const renderStatusCode = (request, response, statusCode) => {
    const httpStatusCode = http.STATUS_CODES[statusCode.toString()];
    if (httpStatusCode !== undefined) {
        response.writeHead(statusCode, {
            "Content-Type": "application/json",
        });
        response.write(`Status Code Response: ${httpStatusCode}`);
        response.end();
    } else {
        const error = "Status code is invalid";
        handleResourceNotFoundError(request, response, error);
        response.end();
    }
};

const renderDelayResponse = (request, response, delay) => {
    setTimeout(() => {
        response.writeHead(200, {
            "Content-Type": "text/plain"
        });
        response.write(`Response delayed for ${delay} seconds`);
        response.end();
    }, delay * 1000);
};

const render404NotFound = (request, response) => {
    readFilePromise("./404NotFound.html")
        .then((data) => {
            response.writeHead(404, {
                "Content-Type": "text/html",
            });
            response.write(data);
            response.end();
        })
        .catch((error) => {
            handleResourceNotFoundError(request, response, error);
            response.end();
        });
};
const readFilePromise = (filename) => {
    return new Promise((resolve, reject) => {
        fs.readFile(filename, "utf8", (error, data) => {
            if (error) {
                reject(error);
            } else {
                resolve(data);
            }
        });
    });
};

const handleResourceNotFoundError = (request, response, error) => {
    response.writeHead(500, {
        "Content-Type": "application/json",
    });
    const errorMessage = {
        message: "Internal Server Error",
        error,
    };
    response.write(JSON.stringify(errorMessage));
    response.end();
};

module.exports = {
    renderHtml,
    renderJson,
    renderUuid,
    renderStatusCode,
    renderDelayResponse,
    render404NotFound,
    renderCss,
};
